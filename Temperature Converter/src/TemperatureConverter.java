import java.util.*;


public class TemperatureConverter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Temperature Conversion \n========================");
		Scanner userInput  = new Scanner(System.in);
		
		System.out.println("What unit are you beginning with? (K)elvin, (F)ahrenheit 0r (C)elsius: ");
		String userInitialUnit = userInput.nextLine();
		
		System.out.println("What unit would you like to convert to? (K)elvin, (F)ahrenheit 0r (C)elsius: ");
		String userConvertUnit = userInput.nextLine();
		
		System.out.println("What is the inital temperature (In Double Form): ");
		double temperature = userInput.nextDouble();
		
		double finalConversion = convert(userInitialUnit, userConvertUnit, temperature);
		System.out.println("Your temperature in " + userConvertUnit + ": " + finalConversion);
	}
	
	public static double convert(String initialUnit, String convertUnit, double initialTemperature) {
		if(initialUnit.contains("F") && convertUnit.contains("C")) {
			double convertToCelcius = (initialTemperature - 32) * 5/9;
			return convertToCelcius;
		}
		
		else if(initialUnit.contains("C") && convertUnit.contains("F")){
			double convertToFahrenheit = (9 * initialTemperature/5) + 32;
			return convertToFahrenheit;
		}
		
		else if(initialUnit.contains("C") && convertUnit.contains("K")) {
			double convertToKelvin = initialTemperature + 273;
			return convertToKelvin;
		}
		else if(initialUnit.contains("K") && convertUnit.contains("C")) {
			double convertToCelcius = initialTemperature - 273;
			return convertToCelcius;
		}
		else if(initialUnit.contains("F") && convertUnit.contains("K")) {
			double convertToCelcius = (initialTemperature - 32) * 5/9;
			double convertToKelvin = convertToCelcius + 273;
			return convertToKelvin;
		}
		else if(initialUnit.contains("K") && convertUnit.contains("F")) {
			double convertToCelcius = initialTemperature - 273;
			double convertToFarenheit = (9 * convertToCelcius/5) + 32;
			return convertToFarenheit;
		}
		else {
			return 0;
		}
	}

}
