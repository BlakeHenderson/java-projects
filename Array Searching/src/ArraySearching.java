import java.util.Arrays;
import java.util.Scanner;

public class ArraySearching {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Array Searching");
		System.out.println("==================");
		System.out.println("How many numbers do you want to generate?: ");
		
		int numTimes = userInput.nextInt();
		
		int[] randomNumbers = new int[numTimes];
		
		System.out.println("Generating Numbers...");
		
		for(int i = 0; i < numTimes; i++){
			randomNumbers[i] = (int)(Math.random() * 10);
			System.out.print(".");
			Thread.sleep(200);
		}
		
		System.out.println("\nYour list has been created");
		
		System.out.println("What number do you want to try and find?: ");
		int numFind = userInput.nextInt();
		
		boolean foundNum = false;
		for(int j = 0; j < randomNumbers.length; j++){
			if(randomNumbers[j] == numFind){
				foundNum = true;
			}
		}
		
		if(foundNum == true){
			System.out.println("Number Found!");
			System.out.println("Here is the original list.");
			System.out.println(Arrays.toString(randomNumbers));
		}
		else{
			System.out.println("That number was not found");
			System.out.println("Here is the original list.");
			System.out.println(Arrays.toString(randomNumbers));
		}
	}

}
