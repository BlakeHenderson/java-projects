import java.util.*;

public class Craps {
	
	private static int moneyBet = 0;
	private static int moneyLost = 0;
	private static int totalMoneyWon = 0;
	private static int playerTotalDice = 0;
	private static boolean Pass = false;

	public static void main(String[] args) {
		boolean run = true; 
		
		System.out.println("Welcome to Electronic Craps.");
		System.out.println("Where you can win and lose big fantasy money.");
		System.out.println("---------------------------------------------");
		
		while(run) {
			betting();
			dice();
			comeOut();
		}
	}
	
	public static void pass() {
		Scanner userInput = new Scanner(System.in);
		boolean run = true;
		
		while(run) {
			System.out.println("Would you like bet the 'pass' or 'don't pass'? (P/D): ");
			String passOrNo = userInput.nextLine();
			
			if(passOrNo.toUpperCase().charAt(0) == 'P'){
				run = false;
				Pass = true;
			}
			else if(passOrNo.toUpperCase().charAt(0) == 'D'){
				run = false;
			}
			else{
				System.out.println("Please try again with a P or D");
			}
		
		}
		
	}
	
	public static void betting(){
		Scanner userInput = new Scanner(System.in);
		boolean run = true;
		
		System.out.println("How much would you like to bet?: ");
		int userBet = userInput.nextInt();
		
		if(userBet > 0){
			System.out.println("Is $" + userBet + " the correct amount? (Y/N):");
			String correctAmount = userInput.next();
			
			if(correctAmount.toUpperCase().charAt(0) == 'N'){
				betting();
			}
			else if(correctAmount.toUpperCase().charAt(0) == 'Y') {
				moneyBet = userBet;
				pass();
			}
		}
		else{
			System.out.println("Please input a amount above $0");
			betting();
		}
		
	}
	
	public static void dice() {
		int randomDiceRoll1 = randInt(1, 6);
		int randomDiceRoll2 = randInt(1, 6);
		
		System.out.println("Your first die rolled: " + randomDiceRoll1);
		System.out.println("Your second die rolled: " + randomDiceRoll2);
		System.out.println("Your total dice rolled was: " + (randomDiceRoll1 + randomDiceRoll2));

		playerTotalDice = randomDiceRoll1 + randomDiceRoll2;
	}
	
	public static void comeOut() {
		int randomInteger = randInt(1, 12);
		if(randomInteger == 2 || randomInteger == 3 || randomInteger == 12){
			if(Pass == true) {
				System.out.println("Craps! You Lose!");
				System.out.println("You lost: -" + moneyBet);
			}
			else {
				totalMoneyWon = totalMoneyWon + moneyBet;
				System.out.println("You Won " + totalMoneyWon *2 + " The game ends!");
			}
		}
		
		else if(randomInteger == 7 || randomInteger == 11){
			if(Pass == true) {
				System.out.println("You Won! but you continue! Heres comes the next roll...");
				comeOut();
			}
		}
		
		else {
			point(randomInteger);
		}
	}
	
	public static void point(int randomInteger) {
		
		if(randomInteger == playerTotalDice){
			if(Pass == true){
				totalMoneyWon = totalMoneyWon + moneyBet;
				System.out.println("You Win! You won a total of $" + totalMoneyWon + " The Game is Over.");
			}
			else {
				System.out.println("You Lose! You won no money");
			}
		}
		else if(randomInteger != playerTotalDice) {
			if(Pass == false){
				totalMoneyWon = totalMoneyWon + moneyBet;
				System.out.println("You Win! You won a total of $" + totalMoneyWon + " The Game is Over.");
			}
		}
		else{
			if(randomInteger == 7){
				if(Pass = true) {
					System.out.println("You Lose! You won no money");
				}
			}
			totalMoneyWon = totalMoneyWon + moneyBet;
			System.out.println("You Win Money! You won $" + totalMoneyWon + " The Game is Over.");
		}
		
	}
	
	
	public static int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
}
