import java.util.Scanner;
import java.util.Arrays;

public class Hangman {
	
	static boolean run = true;
	static Scanner userInput = new Scanner(System.in);

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		do {
			int userSelection = menu();
			if(userSelection == 1){
				System.out.println("Generating Singleplayer Match...");
				Thread.sleep(200);
				lines();
				singlePlayer();
			}
			else if(userSelection == 2){
				System.out.println("Generating Multiplayer Match...");
				Thread.sleep(200);
				lines();
				multiPlayer();
			}
			else{
				run = false;
				System.out.println("Exiting Program....");
				System.exit(0);
			}
			
		}while(run);
			
	}
	
	//Main Menu
	public static int menu(){
		System.out.println("Welcome to Hangman!");
		System.out.println("========================");
		System.out.println("(1) - Single Player");
		System.out.println("(2) - Multiplayer");
		System.out.println("(3) - Exit Game");
		System.out.println("Enter a Selection: ");
		
		int userSelection = userInput.nextInt();
		return userSelection;
	}
	
	//This method creates a environment for a single player game
	public static void singlePlayer(){
		String[] words = new String[10];
		
		//Define words (for now)
		words[0] = "dog";
		words[1] ="cat";
		words[2] ="box";
		words[3] ="music";
		words[4] ="computer";
		words[5] ="phone";
		words[6] ="charger";
		words[7] ="paper";
		words[8] ="pen";
		words[9] ="monitor";
		
		//Set Game Runtime
		boolean game = true;
		
		//Random Word for selection
		String randomWord = words[(int)(Math.random() * 10)];
		
		//Create Selected Word Array
		String[] selectedLetters = new String[26];
		
		//Split word into a Array
		String[] splitWord = new String[randomWord.length()];
		for(int j = 0; j < randomWord.length(); j++){
			splitWord[j] = "_";
		}
		
		//Play welcome message
		System.out.println("Welcome to Singleplayer Hangman!");
		System.out.println("Please guess a letter for the " + randomWord.length() + " letter word!\n");
		
		//Declare Lives
		int lives = 5;
		
		//Declare a temporary word storage
		String tempWord = "";
		
		//Declare a Counter
		int nextIndex = 0;
		
		//Generate beginning hangman ASCII
		hangmanASCII(lives);
		
		boolean alreadyGuessed = false;
		
		while(game){
			if(lives != 0){
				//Check to see if they won!
				
				for(int j = 0; j < randomWord.length(); j++){
					tempWord += splitWord[j];
				}

				if(tempWord.equals(randomWord)){
					break;
				}
				else{
					tempWord = "";
				}
				
				System.out.println(Arrays.toString(splitWord));
				System.out.println("Enter a Selection: ");
				String userLetterSelection = userInput.next();
				
				//Check if it has already been guessed
				if(Arrays.asList(selectedLetters).contains(userLetterSelection)){
					alreadyGuessed = true;
				}
				
				
				if(alreadyGuessed == false){
					//Add it to already guessed list
					selectedLetters[nextIndex] = userLetterSelection;
					nextIndex++;
					
					//Check Selection
					int count = 0;
					for(int k = 0; k < randomWord.length(); k++){
						if(randomWord.charAt(k) == userLetterSelection.charAt(0)){
							splitWord[k] = userLetterSelection;
							count++;
						}
					}
					
					if(count == 0){
						lives--;
						System.out.println("Oh No! There was no " + userLetterSelection + " in the word!");
						System.out.println("You added more the hangman! You have " + lives + " left!");
						hangmanASCII(lives);
					}
					else {
						System.out.println("The letter you guessed appeared " + count + " times!");
						hangmanASCII(lives);
					}
				}
				else{
					System.out.println("You've already guessed that letter! Guess again. ");
					hangmanASCII(lives);
					alreadyGuessed = false;
				}
			}
			else {
				System.out.println("Game Over! You guessed the word incorrectly!");
				System.out.println("The word was: " + randomWord);
				return;
			}
		}
		System.out.println("You Won! Thank you for playing:)");
		System.out.println("The word was: " + randomWord + "\n");
	}
	
	//This method creates a environment for a multiplayer game
	public static void multiPlayer(){
		
		//Set Game Runtime
		boolean game = true;
		
		//Create Selected Word Array
		String[] selectedLetters = new String[26];
		
		//Play welcome message
		System.out.println("Welcome to Multiplayer Hangman!");
		System.out.println("Player 1 Enter a Word for the other player:");
		String randomWord = userInput.next();
		
		//Split word into a Array
		String[] splitWord = new String[randomWord.length()];
		for(int j = 0; j < randomWord.length(); j++){
			splitWord[j] = "_";
		}
		
		//Declare Lives
		int lives = 5;
		
		//Declare a temporary word storage
		String tempWord = "";
		
		//Declare a Counter
		int nextIndex = 0;
		
		//Insert Lines
		lines();
		
		//Generate beginning hangman ASCII
		hangmanASCII(lives);
		
		boolean alreadyGuessed = false;
		
		
		while(game){
			if(lives != 0){
				//Check to see if they won!
				
				for(int j = 0; j < randomWord.length(); j++){
					tempWord += splitWord[j];
				}

				if(tempWord.equals(randomWord)){
					break;
				}
				else{
					tempWord = "";
				}
				
				System.out.println(Arrays.toString(splitWord));
				System.out.println("Enter a Selection: ");
				String userLetterSelection = userInput.next();
				
				//Check if it has already been guessed
				if(Arrays.asList(selectedLetters).contains(userLetterSelection)){
					alreadyGuessed = true;
				}
				
				
				if(alreadyGuessed == false){
					//Add it to already guessed list
					selectedLetters[nextIndex] = userLetterSelection;
					nextIndex++;
					
					//Check Selection
					int count = 0;
					for(int k = 0; k < randomWord.length(); k++){
						if(randomWord.charAt(k) == userLetterSelection.charAt(0)){
							splitWord[k] = userLetterSelection;
							count++;
						}
					}
					
					if(count == 0){
						lives--;
						System.out.println("Oh No! There was no " + userLetterSelection + " in the word!");
						System.out.println("You added more the hangman! You have " + lives + " left!");
						hangmanASCII(lives);
					}
					else {
						System.out.println("The letter you guessed appeared " + count + " times!");
						hangmanASCII(lives);
					}
				}
				else{
					System.out.println("You've already guessed that letter! Guess again. ");
					hangmanASCII(lives);
					alreadyGuessed = false;
				}
			}
			else {
				System.out.println("Game Over! You guessed the word incorrectly!");
				System.out.println("The word was: " + randomWord);
				return;
			}
		}
		System.out.println("You Won! Thank you for playing:)");
		System.out.println("The word was: " + randomWord + "\n");
	}
	
	//Simulates a "thinking" computer with the Thread.sleep() function
	public static void think() throws InterruptedException{
		Thread.sleep(200);
	}
	
	//Creates a ASCII art on the console for the user to view
	public static void hangmanASCII(int value){
		String image;
		switch (value)
		{
			case 0:	 image = " _____\n" +
							" |/  |\n" +
							" |   0\n" +
							" |  -O-\n" +
							" |   \" \n" +
							" |\n" +
							" |\n" +
			
							"========\n";
					System.out.println(image);
					break;
				
			case 1:	image = " _____\n" +
							" |/  |\n" +
							" |   0\n" +
							" |  -O-\n" +
							" |\n" +
							" |\n" +
							" |\n" +
							"========\n";
					System.out.println(image);
					break;
		
			case 2:	image = " _____\n" +
							" |/  |\n" +
							" |   0\n" +
							" |  -O\n" +
							" |\n" +
							" |\n" +
							" |\n" +
							"========\n";
					System.out.println(image);
					break;
			
			case 3: image = " _____\n" +
							" |/  |\n" +
							" |   0\n" +
							" |   O\n" +
							" |\n" +
							" |\n" +
							" |\n" +
							"========\n";
					System.out.println(image);
					break;
			
			case 4: image = " _____\n" +
							" |/  |\n" +
							" |   0\n" +
							" |\n" +
							" |\n" +
							" |\n" +
							" |\n" +
							"========\n";
					System.out.println(image);
					break;
			
			case 5: image = " _____\n" +
							" |/  |\n" +
							" |\n" +
							" |\n" +
							" |\n" +
							" |\n" +
							" |\n" +
							"========\n";
					System.out.println(image);
					break;
		}		
		
		
	}
	
	
	//Generates lines to display if the user guessed the correct letters
	public static void lines(){
		for(int i = 0; i < 100; i++){
			System.out.println("");
		}
	}

}
