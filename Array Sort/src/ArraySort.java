import java.util.Scanner;

public class ArraySort {

	public static int[] numbers = new int[20];
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.println("Array Sort");
		System.out.println("==========================================================");
		System.out.println("How many numbers would you like to enter? (Min 3, Max 20): ");
		int numTimes = userInput.nextInt();
		
		
		if(numTimes >= 3 && numTimes <= 20){
			for(int i = 0; i < numTimes; i++){
				System.out.println("Please Enter Number " + (i+1));
				numbers[i] = userInput.nextInt();
			}
			
			sort();
			
			System.out.println("Numbers in Order:");
			for(int j = 0; j < numbers.length; j++){
				if(numbers[j] != 0){
					System.out.println(numbers[j]);
				}
			}
		}
		else {
			System.out.println("Please enter a number between 3-20");
		}
		
	}
		
	public static void sort() {
		int temp = 0;
		for (int j = 0; j < numbers.length - 1; j++){
			for (int i = 0; i < numbers.length - 1; i++){
				if(numbers[i] > numbers[i + 1]){                   
					temp = numbers[i];
					numbers[i]= numbers[i + 1];
					numbers[i + 1] = temp;
				}      
			}
		}
	}
}
